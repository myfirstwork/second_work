var gulp = require('gulp'),
    sass = require('gulp-sass'),
    mustache = require('gulp-mustache');
 
gulp.task('default', function () {
  return gulp.src('./src/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./public/styles'));
});
